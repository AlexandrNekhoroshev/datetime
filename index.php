<?php
/**
 * Class CompareDate
 *
 * compare to difference date
 */
Class CompareDate
{
    /**
     * @var int $years - year difference
     * @var int $months - month difference
     * @var int $days - days difference
     * @var int $totalDays  - total days difference
     * @var bool $invert - is first date bigger then second date
     * @var array() $firstDate - exploded first date
     * @var array() $secondDate - exploded second date
     */
    public $years;
    public $months;
    public $days;
    public $totalDays;
    public $invert = false;
    protected $firstDate;
    protected $secondDate;
    /**
     * CompareDate constructor.
     * @param string $fistDate
     * @param string $secondDate
     */
    public function __construct($fistDate, $secondDate = '')
    {
        $this->firstDate = $fistDate;
        $this->secondDate = $secondDate ? $secondDate : date("Y-m-d", time());
        $this->makeCompare();
    }
    private function makeCompare()
    {
        $this->strToDates(['first', 'second']);
        $this->isFirstBigger();
        $this->calcDifference();
        $this->getTotalDays();
    }
    /**
     * Explode string to assoc date array
     *
     * @param array $propList
     */
    private function strToDates($propList = array())
    {
        foreach ($propList as $prop) {
            list($year, $month, $day) = explode('-', $this->{$prop.'Date'});
            $this->{$prop.'Date'} = ['year' => $year, 'month' => $month, 'day' => $day];
        }
    }
    /**
     * Check is first date bigger than second
     */
    private function isFirstBigger()
    {
        $secondDate = $this->secondDate;
        foreach ($this->firstDate as $key => $item) {
            if ($item > $secondDate[$key]) {
                $this->invert = true;
                return;
            }
        }
    }
    /**
     * Calculate difference
     *
     */
    private function calcDifference()
    {
        $firstDate = $this->firstDate;
        foreach ($this->secondDate as $key => $item) {
            $diff = $item - $firstDate[$key];
            $this->{$key.'s'} = $this->invert ? abs($diff) : $diff;
        }
    }
    private function getTotalDays()
    {
        $this->totalDays = $this->years * 365 + $this->months * 30 + $this->days;
    }
}
if (isset($_POST['submit'])) {
    $date = new CompareDate($_POST['first'], $_POST['second']);
    echo "Между датами ", $date->years, ' лет, ', $date->months, ' месяцев, ', $date->days, ' дней.';
    echo "<br>";
    echo "Всего дней - ", $date->totalDays;
    echo "<br>";
    echo !$date->invert ? 'Вторая' : 'Первая', ' дата больше';
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Подсчет разницы между датами</title>
</head>
<body>
    <div class="container" style="padding: 30px">
        <form method="POST">
            <div>
                <input type="date" name="first" placeholder="Первая дата">
            </div>
            <div>
                <input type="date" name="second" placeholder="Вторая дата">
            </div>
            <div>
                <input type="submit" name="submit" value="Подсчитать">
            </div>
        </form>
    </div>
</body>
</html>
